<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>



    <!-- Styles -->
    <!-- Latest compiled and minified CSS -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/step.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/font.css') }}" rel="stylesheet">
    <link href="{{ asset('css/sli.css') }}" rel="stylesheet">


</head>

<body style="background:#546e7a;">
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top" style="margin-bottom:0px; border-bottom:1px solid #00695c; ">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ route('login') }}" style="margin-top:5px;">
                        HOME
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a href="{{ route('login') }}">Login</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" styel="position:relative; padding-left:0px;">
                                    <img src="/uploads/avatars/{{ Auth::user() -> avatar }}" style="width:32px; height:32px;  top:10px;  border-radius:50%;"> 
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a class="btn" href="{{ route('profile')  }}">
                                            Profile
                                        </a>
                                    </li>
                                    <li>
                                        <a class="btn" href="{{ route('check')  }}">
                                            Equipments
                                        </a>
                                    </li>
                                    <li>
                                        <a class="btn" href="{{ route('viewus')  }}">
                                            About us
                                        </a>
                                    </li>

                                    <li>
                                        <a class="btn" href="{{ route('peopview')  }}">
                                            Employs
                                        </a>
                                    </li>
                                    
                                    <li>
                                        <a href="{{ route('editwork')  }}" class="btn">
                                            Achivments
                                        </a>
                                    </li>    
                                    <li>
                                        <a class="btn-primary btn" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/step.js') }}"></script>
    <script src="{{ asset('js/sli.js') }}"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    
    
</body>
</html>
